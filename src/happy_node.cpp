#include "rclcpp/rclcpp.hpp"
//happy worldとROSINFOで表示するノード

class Happy : public rclcpp::Node{ //rclcppからNodeクラスを継承
    public: //クラスの外で使うときにpublicを付けて継承する
        Happy() : Node("happy_node"){ //スーパークラスのNodeコンストラクタ呼び出し。ノードの定義をする。
            RCLCPP_INFO(this->get_logger(),"hello world!"); //ログ情報を出力
        } //thisポインターで継承元のNodeクラスのメンバーにアクセス
};

int main(int argc, char **argv){
    rclcpp::init(argc, argv); //ROS2通信を初期化
    auto node = std::make_shared<Happy>(); //Happyが継承したNodeクラスの初期化をすることでノードを生成
    //rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
