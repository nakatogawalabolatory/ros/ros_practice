#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

class HappySubscriber : public rclcpp::Node {
    public:
        HappySubscriber() : Node("happy_subscriber_node"){
            subscriber_ = this->create_subscription<std_msgs::msg::String>(
            "topic",1,std::bind(&HappySubscriber::callback, this,std::placeholders::_1));
        }

        void callback(const std_msgs::msg::String::SharedPtr msg){
            RCLCPP_INFO(this->get_logger(),"サブスクリプション：%s", msg->data.c_str());
        }
    private:
        rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscriber_;
};

int main(int argc, char **argv){
    rclcpp::init(argc, argv);
    auto node = std::make_shared<HappySubscriber>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
