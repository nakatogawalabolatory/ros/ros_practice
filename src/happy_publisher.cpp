#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp" //文字列のメッセージを送るためにStringクラスをインポートする
#include<iostream>
#include<chrono>

using namespace std;

class HappyPublisher : public rclcpp::Node{
    public:
        HappyPublisher() : Node("happy_publisher_node"){ //コンストラクタにNodeクラスを継承,ノードを初期化
            Publisher_ = this->create_publisher<std_msgs::msg::String>("topic",1);
            timer_ = this->create_wall_timer(1000ms, bind(&HappyPublisher::timer_callback,this)); //一定周期でパブリッシュするときに生成
            count_ = 0;
        }

        void timer_callback(){ //コンストラクタからコールバックされて呼び出される
            auto msg = std_msgs::msg::String();
            msg.data = "ハッピーワールド" + to_string(count_++); //String型のメッセージをトピックを介して送る
            Publisher_->publish(msg);
            RCLCPP_INFO(this->get_logger(),"パブリッシング：%s",msg.data.c_str());//Ｃ言語文字列の先頭を返すポインター
        }

    private:
        rclcpp::TimerBase::SharedPtr timer_;
        rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Publisher_;
        int count_;
};

int main(int argc, char **argv){
    rclcpp::init(argc, argv);
    auto node = make_shared<HappyPublisher>();
    rclcpp::spin(node);
    rclcpp::shutdown();
}
