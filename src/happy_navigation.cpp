#include"rclcpp/rclcpp.hpp"
#include<geometry_msgs/msg/twist.hpp>
#include<nav_msgs/msg/odometry.hpp>
#include<tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include<tf2/LinearMath/Quaternion.h>
#include<chrono>
#include<cmath>

using namespace std::chrono_literals;
//using namespace std;

class HappyNavigation : public rclcpp::Node {
    public:
        HappyNavigation() : Node("happy_navigation_node"){
            pub = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel",10);
            sub = this->create_subscription<nav_msgs::msg::Odometry>("odom", 
                            10, std::bind(&HappyNavigation::odom_callback, this, std::placeholders::_1));
            timer_ = this->create_wall_timer(std::chrono::milliseconds(1000), std::bind(&HappyNavigation::timer_callback, this));

            //goal_x=goal_y=goal_yaw=0.0; // 初期位置とゴールを初期化
            start_x= start_y= start_yaw = 0.0;

            //auto cmd_vel = geometry_msgs::msg::Twist();
            cmd_vel.linear.x = 0.0;
            cmd_vel.angular.z = 0.0; // スピードと向きをセット
        }

        std::tuple<double, double, double> get_pose(const nav_msgs::msg::Odometry::SharedPtr msg){
            double x = msg->pose.pose.position.x;
            double y = msg->pose.pose.position.y;
            tf2::Quaternion q;
            tf2::fromMsg(msg->pose.pose.orientation, q);
            double roll=0.0, pitch=0.0, yaw = 0.0;
            tf2::Matrix3x3(q).getRPY(roll, pitch, yaw);
            return std::make_tuple(x, y, yaw);
        }

        void odom_callback(const nav_msgs::msg::Odometry::SharedPtr msg){
            auto [goal_x, goal_y, goal_yaw] = get_pose(msg);
            RCLCPP_INFO(this->get_logger(), "x= %.2f[m], y=%.2f[m] yaw=%.2f[rad/s]", goal_x, goal_y, goal_yaw);
        }

        bool move_distance(double dist){
            double error = 0.05; // 許容誤差 [m]
            double diff = dist - std::sqrt(std::pow(goal_x - start_x, 2) + std::pow(goal_y - start_y, 2));
            if (std::abs(diff) > error) {
                cmd_vel.linear.x = 0.25;
                cmd_vel.angular.z = 0.0;
                return false;
            }
            else {
                cmd_vel.linear.x = 0.0;
                cmd_vel.angular.z = 0.0;
                return true;
            }
        }

        bool rotate_angle(double angle){
            double diff_yaw = angle - (goal_yaw - start_yaw);
            if(std::abs(diff_yaw) > 0.05){
                cmd_vel.angular.z = 0.25;
                cmd_vel.linear.x = 0.0;
                return false;
            }
            else {
                cmd_vel.linear.x = 0.0;
                cmd_vel.angular.z = 0.0;
                return true;
            }
        }

        void timer_callback(){
            //auto cmd_vel = geometry_msgs::msg::Twist();
            pub->publish(cmd_vel);
        }

        void happy_move(double distance, double angle){
            int state = 0;
            while(rclcpp::ok()){
                this->move_distance(distance);
                this->rotate_angle(angle);
                RCLCPP_INFO(this->get_logger(), "動きます！%.2f",cmd_vel.linear.x);
                if(state == 0){
                    if(move_distance(distance)){
                        state = 1;
                    }
                }
                else if(state == 1){
                    if(rotate_angle(angle)){
                        continue;
                    }
                }
                else{
                    RCLCPP_WARN(this->get_logger(), "エラー状態です!!");
                }
            }
            RCLCPP_INFO(this->get_logger(), "終了です");
        }

    private:
        rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr pub;
        rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr sub;
        rclcpp::TimerBase::SharedPtr timer_;
        nav_msgs::msg::Odometry goal_pose, start_pose;
        geometry_msgs::msg::Twist cmd_vel;
        double start_x = start_pose.pose.pose.position.x;
        double start_y = start_pose.pose.pose.position.y;
        double start_yaw = start_pose.pose.pose.orientation.z;
        double goal_x = goal_pose.pose.pose.position.x;
        double goal_y = goal_pose.pose.pose.position.y;
        double goal_yaw = goal_pose.pose.pose.orientation.z;
};

int main(int argc, char** argv){
    rclcpp::init(argc, argv);
    auto node = std::make_shared<HappyNavigation>();
    node->happy_move(2.0, 1.5708);
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
