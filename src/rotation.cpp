#include"rclcpp/rclcpp.hpp"
#include"geometry_msgs/msg/twist.hpp"
#include<unistd.h>
#include<chrono>

class Rotation :public rclcpp::Node{
    public:
        Rotation() : Node("rotation_node"){
            pub = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel",10);
			this->turtle_run();
        }

        void turtle_run(){
            auto twist = geometry_msgs::msg::Twist();
			int i = 0;
			const double pi=3.14159265358979;
			double degree = 90 * pi / 180;
			while(i < 10){
			  if(i % 2 == 0){
                twist.linear.x = 0.2;
                twist.angular.z = 0;
                pub->publish(twist); //直進する命令
                RCLCPP_INFO(this->get_logger(),"直線のパブリッシュ：%f[m/s]",twist.linear.x);
			  }
			  else{
                twist.linear.x = 0;
                twist.angular.z = degree;
                pub->publish(twist); //回転する命令
                RCLCPP_INFO(this->get_logger(),"回転のパブリッシュ：%f[m/s]",twist.angular.z);
			  }
			  i++;
			  usleep(1000000);
			}
			std::cout << "しゅうりょー！" << std::endl;
        
		twist.linear.x = twist.angular.z = 0;
	  	pub->publish(twist);
		}

    private:
        rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr pub; //スマートポインターで、newしなくてもメモリ管理を自動で行ってくれる
};

int main(int argc, char **argv){
    rclcpp::init(argc,argv);
    auto node = std::make_shared<Rotation>(); //std::shared_ptr<Twist>()でもできるが、ちょっと面倒なのでmake_sharedを使う
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
