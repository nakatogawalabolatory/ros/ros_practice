#include"rclcpp/rclcpp.hpp"
#include"geometry_msgs/msg/twist.hpp"
#include<iostream>
#include<chrono>

using namespace std;

class TwistPub :public rclcpp::Node{
    public:
        TwistPub() : Node("twist_pub"){
            pub = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel",10);
            timer_ = this->create_wall_timer(1000ms, bind(&TwistPub::timer_callback, this));
        }

        void timer_callback(){ //time.sleepのようなやつを入れればcallbackで周期的にしなくてもいける？
            auto twist_1 = geometry_msgs::msg::Twist();
            auto twist_2 = geometry_msgs::msg::Twist();
			int i = 0;
			while(i < 5){
			  if(i % 2 == 0){
                twist_1.linear.x = 0.2;
                twist_1.angular.z = 0;
                pub->publish(twist_1); //直進する命令
                RCLCPP_INFO(this->get_logger(),"パブリッシュ：%f[m/s]",twist_1.linear.x);
			  }
			  else{
                twist_2.linear.x = 0;
                twist_2.angular.z = 0.5;
                pub->publish(twist_2); //回転する命令
                RCLCPP_INFO(this->get_logger(),"パブリッシュ：%f[m/s]",twist_2.angular.z);
			  }
			  i++;
			}
            cout << "しゅうりょー！" << endl;
        }
        
    private:
        rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr pub; //スマートポインターで、newしなくてもメモリ管理を自動で行ってくれる
        rclcpp::TimerBase::SharedPtr timer_;
};

int main(int argc, char **argv){
    rclcpp::init(argc,argv);
    auto node = std::make_shared<TwistPub>(); //std::shared_ptr<Twist>()でもできるが、ちょっと面倒なのでmake_sharedを使う
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
