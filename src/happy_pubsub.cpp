#include"rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/int32.hpp"

class HappyPubSub : public rclcpp::Node {
    public:
        HappyPubSub() : Node("happy_pub_sub"){
            publisher_ = this->create_publisher<std_msgs::msg::String>("happy_msg",1);
            subscriber_ = this->create_subscription<std_msgs::msg::Int32>(
            "number",1,std::bind(&HappyPubSub::topic_callback, this, std::placeholders::_1));
        }

        void topic_callback(const std_msgs::msg::Int32::SharedPtr sub_msg){
            auto pub_msg = std_msgs::msg::String();
            RCLCPP_INFO(this->get_logger(),"サブスクリプション：%d",sub_msg->data);
            for(int i=0;i<5;i++){
                pub_msg.data = "ハッピーアクションズ" + std::to_string(count++);
                publisher_->publish(pub_msg);
            }
            RCLCPP_INFO(this->get_logger(),"パブリッシュ：%s",pub_msg.data.c_str());
        }

    private:
        rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
        rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr subscriber_;
        int count = 0;
};

int main(int argc, char **argv){
    rclcpp::init(argc,argv);
    auto node = std::make_shared<HappyPubSub>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
